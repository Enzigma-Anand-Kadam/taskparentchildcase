trigger TaskParentChildCase on Case (before update) {
    if(Trigger.isBefore && Trigger.isUpdate){
        TaskParentChildCase.preventParentFromClosed(Trigger.new, Trigger.oldMap);
    }
}