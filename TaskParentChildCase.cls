/*A case can have multiple child cases, requirement is to restrict user from 
setting the status of parent case to closed, until all the child cases status is closed.*/
public with sharing class TaskParentChildCase{
    public static void preventParentFromClosed(List<Case> lstCase, Map<Id, Case> mapCase){
        
        List<Case> lstCaseId = [SELECT Id, CaseNumber, ParentId FROM Case WHERE ParentId != null AND Id IN : lstCase];
        
        List<Id> lstCaseParentId = new List<Id>();
        
        for(Case objCase : lstCaseId){
            if(objCase.Status == 'Closed' && mapCase.get(objCase.Id).Status != 'Closed'){
                lstCaseParentId.add(objCase.Id);
            }
        }
        List<Case> lstChildList = [SELECT ParentId FROM Case WHERE ParentId IN : lstCaseParentId];
        for(Case objCase : lstChildList){
            if(objCase.Status != 'Closed'){
                mapCase.get(objCase.ParentId).addError('Child status is not closed yet.'); 
            }
        }
    }
}