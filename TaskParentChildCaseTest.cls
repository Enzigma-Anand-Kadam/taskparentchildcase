@isTest
public class TaskParentChildCaseTest {
    @isTest
    static void testpreventParentFromClosed(){
        Case parentCase = new Case(Status = 'Open');
        insert parentCase;
        
        Case childCase = new Case(Status = 'Open', ParentId = parentCase.Id);
        insert childCase;
        
        parentCase.Status = 'Closed';
        try{
            update parentCase;
        }catch(DMLException e){
            System.debug(e.getMessage());
            System.assertEquals(true, e.getMessage().contains('Child status is not closed yet.'), 'Done');
        }
        
    }
}